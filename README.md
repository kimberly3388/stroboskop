# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://kimberly3388@bitbucket.org/kimberly3388/stroboskop.git
cd stroboskop
git add README.md
git commit -a -m "Uveljavitev repozitorija"
```

Naloga 6.2.3:
https://bitbucket.org/kimberly3388/stroboskop/commits/87e208fa997120be354b19155d01aa86e9d36b24

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/kimberly3388/stroboskop/commits/9f6e7f3c5d6ea41fcc2df70aba05475467d727ba

Naloga 6.3.2:
https://bitbucket.org/kimberly3388/stroboskop/commits/f58046467b713c542cdb28863e1fbf541862d155

Naloga 6.3.3:
https://bitbucket.org/kimberly3388/stroboskop/commits/f288fecad744f49b9fdddb50b55ae7e878d05e14

Naloga 6.3.4:
https://bitbucket.org/kimberly3388/stroboskop/commits/848a9ebca6b66f5b0362c06101e40b0dc802f4ad

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/kimberly3388/stroboskop/commits/a99cd85a41ca9c94bce08b0f861840bb72986bdc

Naloga 6.4.2:
https://bitbucket.org/kimberly3388/stroboskop/commits/4b04aedaecc63f137213f20e7d5d27ffb45ee0db

Naloga 6.4.3:
https://bitbucket.org/kimberly3388/stroboskop/commits/17325ea4f517bf4ba0d3e8bba4dc94c75442fd49

Naloga 6.4.4:
https://bitbucket.org/kimberly3388/stroboskop/commits/131bbc601e49f1cddd67067166b041e6419d6364